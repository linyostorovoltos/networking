#include <enet/enet.h>
#include <stdio.h>
#include <cstring>
#include <iostream>



int main (int argc, char ** argv)
{
        if (enet_initialize () != 0)
        {
                fprintf (stderr, "An error occurred while initializing ENet.\n");
                return EXIT_FAILURE;
        }
        atexit (enet_deinitialize);

        ENetHost * client;
        client = enet_host_create (NULL /* create a client host */,
                                   1 /* only allow 1 outgoing connection */,
                                   2 /* allow up 2 channels to be used, 0 and 1 */,
                                   0 /* assume any amount of incoming bandwidth */,
                                   0 /* assume any amount of incoming bandwidth */);
        if (client == NULL)
        {
                fprintf (stderr,
                         "An error occurred while trying to create an ENet client host.\n");
                exit (EXIT_FAILURE);
        }
        ENetAddress address;
        ENetEvent event;
        ENetPeer *peer;
        enet_address_set_host (&address, "127.0.0.1");
        address.port = 1234;

        peer = enet_host_connect (client, &address, 2, 0);
        if (peer == NULL)
        {
                fprintf (stderr,
                         "No available peers for initiating an ENet connection.\n");
                exit (EXIT_FAILURE);
        }

        if (enet_host_service (client, &event, 100000) > 0 &&
            event.type == ENET_EVENT_TYPE_CONNECT)
        {
                puts ("Connection to 127.0.0.1:1234 succeeded.");
        }

        else
        {
                /* Either the 5 seconds are up or a disconnect event was */
                /* received. Reset the peer in the event the 5 seconds   */
                /* had run out without any significant event.            */
                enet_peer_reset (peer);
                puts ("Connection to 127.0.0.1:1234 failed.");
        }


        while (true) {
                enet_host_service(client, &event, 0);
                switch (event.type) {
                case ENET_EVENT_TYPE_CONNECT:
                        printf ("A new client connected from %x:%u.\n",
                                event.peer->address.host,
                                event.peer->address.port);

                        event.peer->data = (void*)"New User";
                        break;

                case ENET_EVENT_TYPE_RECEIVE:
                        printf ("A packet of length %u containing '%s' was "
                                "received from %s on channel %u.\n",
                                event.packet->dataLength,
                                event.packet->data,
                                event.peer->data,
                                event.channelID);

                        /* Clean up the packet now that we're done using it. */
                        enet_packet_destroy (event.packet);
                        break;

                case ENET_EVENT_TYPE_DISCONNECT:
                        //printf ("%s disconected.\n", event.peer -> data);
                        printf ("why did this happen \n", event.peer->data);
                        break;

                }

                std::string message;
                std::cout << "Input: ";
                std::getline (std::cin, message);
                ENetPacket * packet = enet_packet_create (message.c_str(), strlen(message.c_str()) + 1, ENET_PACKET_FLAG_RELIABLE);
                enet_peer_send (peer, 0, packet);

                enet_host_flush(client);
                enet_packet_destroy(packet);

        }

        enet_host_destroy(client);
}
