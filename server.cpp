#include <enet/enet.h>
#include <cstdio>
#include <cstring>
#include <vector>
#include <string>
#include <iostream>

int main (int argc, char ** argv)
{
        if (enet_initialize () != 0)
        {
                fprintf (stderr, "An error occurred while initializing ENet.\n");
                return EXIT_FAILURE;
        }

        atexit (enet_deinitialize);
        ENetAddress address;
        ENetHost * server;
        /* Bind the server to the default localhost.     */
        /* A specific host address can be specified by   */
        /* enet_address_set_host (& address, "x.x.x.x"); */
        address.host = ENET_HOST_ANY;

        /* Bind the server to port 1234. */
        address.port = 1234;
        server = enet_host_create (&address /* the address to bind the server host to */,
                                   32 /* allow up to 32 clients and/or outgoing connections */,
                                   2 /* allow up to 2 channels to be used, 0 and 1 */,
                                   0 /* assume any amount of incoming bandwidth */,
                                   0 /* assume any amount of outgoing bandwidth */);
        if (server == NULL)
        {
                fprintf (stderr,
                         "An error occurred while trying to create an ENet server host.\n");
                exit (EXIT_FAILURE);
        }

        int connectedPeople;

        ENetEvent event;
        while (true)
        {
                enet_host_service (server, &event, 0);
                switch (event.type)
                {
                case ENET_EVENT_TYPE_CONNECT:
                {
                        printf ("A new client connected from %x:%u.\n", event.peer->address.host, event.peer->address.port);
                        /* Store any relevant client information here. */
                        std::string newString = "User " + std::to_string(connectedPeople);
                        event.peer->data = (void*)newString.c_str();
                        connectedPeople++;
                        break;
                }
                case ENET_EVENT_TYPE_RECEIVE:
                {
                        printf ("%s: %s \n", event.peer->data, event.packet->data, event.packet->dataLength, event.channelID);
                        enet_host_broadcast (server, 0, event.packet);
                        /* Clean up the packet now that we're done using it. */
                        enet_packet_destroy (event.packet);
                        break;
                }

                case ENET_EVENT_TYPE_DISCONNECT:
                        printf ("%s disconnected.\n", event.peer->data);
                        connectedPeople = connectedPeople - 1;
                        /* Reset the peer's client information. */
                        event.peer->data = NULL;
                }
        }
        enet_host_destroy(server);
}
