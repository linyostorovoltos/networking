server: server.cpp
	g++ -c server.cpp
	g++ server.o -o enet-test -lenet
	rm *.o
client: client.cpp
	g++ -c client.cpp
	g++ client.o -o client-test -lenet -fpermissive
	rm *.o
